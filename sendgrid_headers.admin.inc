<?php
/**
 * @file
 * Sendgrid Headers settings form functions goes here.
 */

/**
 * Sendgrid Headers settings form.
 */
function sendgrid_headers_settings_form() {

  global $conf;

  $key = 'sendgrid_headers';

  $form[$key] = [
    '#type' => 'fieldset',
    '#title' => t('Sendgrid headers settings'),
  ];

  $fieldset = &$form[$key];

  $fieldset['sendgrid_headers_category'] = [
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#description' => t('Email category header for Sendgrid (comma separated values can be provided)'),
    '#default_value' => variable_get('sendgrid_headers_category', $conf['base_url']),
  ];

  $fieldset['sendgrid_headers_override_timezone'] = [
    '#type' => 'checkbox',
    '#title' => t('Override Timezone'),
    '#description' => t('You can override Date header with custom Timezone'),
    '#default_value' => variable_get('sendgrid_headers_override_timezone', 0),
  ];

  $options = array_combine(timezone_identifiers_list(), timezone_identifiers_list());

  $fieldset['sendgrid_headers_timezone'] = [
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Timezone'),
    '#description' => t('Custom Timezone for Date header'),
    '#default_value' => variable_get('sendgrid_headers_timezone', $conf['date_default_timezone']),
    '#states' => [
      'visible' => [
        ':input[name="sendgrid_headers_override_timezone"]' => ['checked' => TRUE],
      ],
    ],
  ];

  return system_settings_form($form);
}
